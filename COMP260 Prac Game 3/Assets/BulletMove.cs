﻿using UnityEngine;
using System.Collections;

public class BulletMove : MonoBehaviour {

	public float speed = 10.0f;
	public Vector3 direction;
	private Rigidbody rigidbody;
	private float Lifetime = 0.0f;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		Lifetime += Time.deltaTime;
		if (Lifetime > 3) {
			Destroy (gameObject);
		}
	}

	void FixedUpdate(){
		rigidbody.velocity = speed * direction;
	}

	void OnCollisionEnter(Collision collision) {
		Destroy (gameObject);
	}
}
