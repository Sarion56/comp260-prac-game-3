﻿using UnityEngine;
using System.Collections;

public class FireBullet : MonoBehaviour {

	public BulletMove bulletPrefab;
	private float Reload = 0.1f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Reload += Time.deltaTime;
		if (Input.GetButtonDown ("Fire1") && Reload > 0.1) {
			BulletMove bullet = Instantiate (bulletPrefab);
			bullet.transform.position = transform.position;

			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			bullet.direction = ray.direction;
			Reload = 0.0f;
		}
	}
}
